# stock_trans

#### 介绍
唐李股票分析-坚持下去
分析股票列表中当日价格低于指定时长（例如90日内，180日内）均价的股票，然后分析分析罗列股票的指定参数，例如季度财报，半年利润率等，购买、持有、卖出意见由人为自行分析，不直接提供意见。

#### 软件版本
version_initial  第一版，无框架，功能未完成

stock_analysis  第二版，采用django框架，重构


#### 软件架构
软件架构说明
django + sqlite3

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
