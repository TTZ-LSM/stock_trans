from django.urls import path,include
from . import views

urlpatterns = [
    path('index',views.Hello_price),
    path('get_stock_list',views.get_stocks_list),
    path('get_stock_trans',views.get_stock_trans),
    path('get_zuidi/<int:days>',views.get_zuidi),
    path('update_stock_trans',views.update_stock_trans)
]
