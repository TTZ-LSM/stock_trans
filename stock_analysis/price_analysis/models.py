from django.db import models

# Create your models here.
class stocks_list(models.Model):
    #id
    id=models.AutoField(primary_key=True)
    #股票代码
    stock_code=models.CharField(max_length=10,unique=True)
    #股票名称
    stock_name=models.CharField(max_length=10,default=None)
    def __str__(self):
        return  str(self.stock_code) + ' , ' + str(self.stock_name)
# 存放每日交易数据
class daily_trans(models.Model):
    id=models.AutoField(primary_key=True)
    # 交易日期
    riQi=models.DateTimeField(null=True)
    # 股票代码
    guPiaoCode=models.ForeignKey(stocks_list,to_field='stock_code',on_delete=models.CASCADE)
    # 股票名称
    guPiaoName=models.CharField(max_length=10,default=None)
    # 收盘价
    shouPan=models.CharField(max_length=10)
    # 最高价
    zuiGao=models.CharField(max_length=10)
    # 最低价
    zuiDi=models.CharField(max_length=10)
    # 开盘价
    kaiPan=models.CharField(max_length=10)
    # 前一个交易日收盘价
    qianShouPan=models.CharField(max_length=10)
    # 涨跌额
    zhangDieE=models.CharField(max_length=10)
    # 涨跌幅度
    zhangDieFu=models.CharField(max_length=10)
    # 换手率
    huanShouLv=models.CharField(max_length=10)
    # 成交量
    chenJiaoLiang=models.CharField(max_length=50)
    # 成交金额
    chengJiaoJinE=models.CharField(max_length=50)
    # 总市值
    zongShiZhi=models.CharField(max_length=50)
    # 流通市值
    liuTongShiZhi=models.CharField(max_length=50)

    class Meta:
        unique_together = (('riQi', 'guPiaoName'),)
# 用于get_zuidi页面前端显示
class cal_data(models.Model):
    # 股票代码
    guPiaoCode = models.CharField(max_length=50)
    # 最低价提升幅度
    up_percent = models.CharField(max_length=10)