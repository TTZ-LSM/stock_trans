# Generated by Django 2.1.7 on 2020-03-23 12:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('price_analysis', '0004_auto_20200323_2017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='daily_trans',
            name='riQi',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterUniqueTogether(
            name='daily_trans',
            unique_together={('riQi', 'guPiaoName')},
        ),
    ]
